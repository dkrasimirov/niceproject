package main

import (
	"database/sql"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

// Bootstrap function. It creates the database celled userdata.
// It creates the database if it does not exists only. Then it creates the table called data
// If they are already created this function does not do anything
func checkDb(username string, password string, endpoint string) {
	conn := username + ":" + password + "@tcp(" + endpoint + ")" + "/"
	DbConnection, err := sql.Open("mysql", conn)
	if err != nil {
		panic(err)
	}

	_, err = DbConnection.Exec("CREATE DATABASE IF NOT EXISTS userdata")
	_, err = DbConnection.Exec("CREATE TABLE IF NOT EXISTS userdata.data (id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255) NOT NULL)")
	if err != nil {
		panic(err)
	}
	DbConnection.Close()

	return
}

func main() {

	var username = os.Getenv("USERNAME")
	var password = os.Getenv("PASSWORD")
	var endpoint = os.Getenv("ENDPOINT")
	checkDb(username, password, endpoint)

	conn := username + ":" + password + "@tcp(" + endpoint + ")" + "/userdata"
	db, _ := sql.Open("mysql", conn)
	var version string
	// Inseart only once if there is already a record query it
	db.QueryRow("SELECT title FROM userdata.data").Scan(&version)
	if version == "" {
		_, err := db.Query("INSERT INTO userdata.data VALUES ( 1, 'Hello from Graylog' )")
		db.QueryRow("SELECT title FROM userdata.data").Scan(&version)

		// if there is an error inserting, handle it
		if err != nil {
			panic(err.Error())
		}
	} else {

		db.QueryRow("SELECT title FROM userdata.data").Scan(&version)

	}

	defer db.Close()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		w.Write([]byte(version))

	})

	http.ListenAndServe(":8080", nil)

}
