variable "s3_bucket" {
  default = "dansbucketrepo80"
}

variable "AWS_REGION" {
  default = "eu-central-1"
}