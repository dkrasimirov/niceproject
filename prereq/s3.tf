resource "aws_s3_bucket" "b" {
  bucket = var.s3_bucket
  acl    = "private"
  

  tags = {
    Name = var.s3_bucket
  }
}

resource "aws_s3_bucket_public_access_block" "baccess" {
  bucket = aws_s3_bucket.b.id

  block_public_acls   = true
  block_public_policy = true
  restrict_public_buckets = true
}
 resource "aws_s3_bucket_object" "tf" {
    bucket = aws_s3_bucket.b.id
    key    = "terraform/"
 }

  resource "aws_s3_bucket_object" "repo" {
    bucket = aws_s3_bucket.b.id
    key    = "repo/"
  }