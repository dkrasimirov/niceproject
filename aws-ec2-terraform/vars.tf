variable "AWS_REGION" {
  default = "eu-central-1"
}
variable "PATH_TO_PRIVATE_KEY" {
  default = "mykey"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "mykey.pub"
}

variable "AMIS" {
  type = map(string)
  default = {
    us-east-1 = "ami-0ed9277fb7eb570c9"
    us-west-2 = "ami-00f7e5c52c0f43726"
    eu-west-1 = "ami-04dd4500af104442f"
    eu-central-1= "ami-05d34d340fb1d89e5"
  }
}


variable "INSTANCE_DEVICE_NAME" {
  default = "/dev/xvdh"
}


variable "RDS_PASSWORD" {
  type        = string
  sensitive   = true
}


variable "MARIADB_VERSION" {
  default = "10.5.12"
  
}

variable "s3_bucket" {
  default = "dansbucketrepo80"
}
