output "ELB" {
  value = aws_lb.my-alb.dns_name
}


output "RDS" {
    value = aws_db_instance.mariadb.endpoint
}

output "jumphost" {
    value = aws_instance.jumphost.public_ip 
}