# Creation of ALB loadbalancer attaching it to the 3 subnets 
resource "aws_lb" "my-alb" {
  name            = "my-alb"
  subnets         = [aws_subnet.myVPC-public-1a.id, aws_subnet.myVPC-public-1b.id,aws_subnet.myVPC-public-1c.id] 
  security_groups = [aws_security_group.alb-securitygroup.id]
  internal           = false
  ## type of the loadbalancer
  load_balancer_type = "application"
 
  
  enable_cross_zone_load_balancing  = true

 

 tags = {
    Name = "my-elb"
  }

}

# creation of the listener on port 80 with attaching it to the TG1 target group
resource "aws_lb_listener" "alb-listener" {
  load_balancer_arn = aws_lb.my-alb.id
   port              = "80"
  protocol          = "HTTP"

   default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.TG1.arn
   }
 }

## creation of target group TG1 and port 8080 matching to the port of the application
resource "aws_lb_target_group" "TG1" {
  name     = "TG1"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.myVPC.id
}
## this steps automatically attach the instances from the ASG
resource "aws_autoscaling_attachment" "asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.example-autoscaling.name
  alb_target_group_arn   = aws_lb_target_group.TG1.arn
  }
 


