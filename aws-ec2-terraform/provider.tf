 
provider "aws" {
  region = var.AWS_REGION
}
terraform {
  backend "s3" {
    #Replace this with your bucket name!
    bucket         = "dansbucketrepo80"
    key            = "terraform/terraform.tfstate"
    region         = "eu-central-1"
  
    
    }
}