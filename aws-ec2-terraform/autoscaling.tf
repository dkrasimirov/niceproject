# Launch configuration for the ASG. Important is the user_data image id was pulled from variables and also attaching ebs volume
resource "aws_launch_configuration" "example-launchconfig" {
  name_prefix     = "example-launchconfig"
  image_id        = var.AMIS[var.AWS_REGION]
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.mykeypair.key_name
  security_groups = [aws_security_group.asg-securitygroup.id]
  iam_instance_profile = data.aws_iam_instance_profile.s3-mybucket-role-instanceprofile.name
  ebs_block_device {
      device_name = var.INSTANCE_DEVICE_NAME
      volume_size = "10"
      volume_type = "gp2"
      delete_on_termination = true
    }

  user_data       = <<-EOT
    #!/bin/bash

  DEVICE=\${var.INSTANCE_DEVICE_NAME}
  FS_TYPE=$(file -s $DEVICE | awk '{print $2}')
  MOUNT_POINT=/data

  # If no FS, then this output contains "data"
  if [ "$FS_TYPE" = "data" ]
  then
    echo "Creating file system on $DEVICE"
    mkfs.xfs $DEVICE
  fi

  mkdir $MOUNT_POINT
  mount $DEVICE $MOUNT_POINT
  echo "${var.INSTANCE_DEVICE_NAME} /data xfs defaults 0 0" >> /etc/fstab
  aws s3 cp s3://${var.s3_bucket}/repo/myapp /data
  chmod 755 /data/myapp
  aws s3 cp s3://${var.s3_bucket}/repo/myapp.service /etc/systemd/system
  echo -e "USERNAME=root\nPASSWORD=${var.RDS_PASSWORD}\nENDPOINT=${aws_db_instance.mariadb.endpoint}" >> /data/.envs
  systemctl enable myapp.service 
  systemctl start myapp.service 

EOT
  lifecycle {
    create_before_destroy = true
  }
}

# Creation of the actual autoscaling group with min of 2 instances and maximum of 4. 
resource "aws_autoscaling_group" "example-autoscaling" {
  name                      = "example-autoscaling"
  # spin up the instances in private subnets
  vpc_zone_identifier       = [aws_subnet.myVPC-private-1a.id, aws_subnet.myVPC-private-1b.id,aws_subnet.myVPC-private-1c.id]
  # Attaching the launch configuration
  launch_configuration      = aws_launch_configuration.example-launchconfig.name
  min_size                  = 2
  max_size                  = 4
  health_check_grace_period = 300
 # Better to have health check of ELB than EC2 to prevent some mismash when ASG thinks instance is missing but it is not missing for ALB
  health_check_type         = "ELB"
  force_delete              = true

  tag {
    key                 = "Name"
    value               = "ec2 instance"
    propagate_at_launch = true
  }
  ## Do not create ASG and instances before RDS and NAT gateways
  depends_on = [
    aws_nat_gateway.nat-gw,
    aws_db_instance.mariadb,
  ]
}
