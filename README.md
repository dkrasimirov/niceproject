## Getting started

Introducing the project with a diagram. The diagram shows full solution with the HA,RDS and LB options. I took in consideration making the application high available, autoscaled, pulling data from AWS RDS and working with autoscaling group with ALB loadbalancer also working with terraform and user data to spin up the autoscalling group. The terraform also creates separate VPC with a public and private subnets. All of the explanations will follow in step by step manner.

The following diagram of the solution.
![Solution Diagram](images/diagram.jpg)

## How it works on a high level
The entry point is the user browsing the application. Here the url can be provided by two ways. First as DNS entry of the ALB which can be found in AWS management console when the ALB has been deployed. It can be found in EC2 section -> then Lad Balancer link on the left and then click on the load Balancer where there will be DNS host to display. Second the DNS entry can be registered in DNS server who host particular zone like Route 53 if there is a public Route 53 zone with alias record pointing to the LB or other public DNS provider.
The User is browsing on port 80 then the request end up to the ALB with the respective target group on port 8080. Then the request goes to the nodes of the autoscaling group on port 8080. The VPC from the diagram has three public subnets in the three AZ (availability zones) and three private subnets in three AZ. ALB has been deployed in the public subnets and the instances from ASG (autoscaling group) in the private subnets. Public subnets communicate through IG(internet gateway), private subnets through NAT gateway.
Couple of security groups has been deployed for protection if the instances and ensuring that only the traffic on port 8080 has been allowed. Then the application communicates with mariadb RDS service deployed in the private subnets of the VPC and allowing only 3306 port only from ASG securitygroup i.e. autoscaling instances. The jumphost  is a single Linux EC2 instance and is only deployed for troubleshooting purposes and should stay shut down. 

## main.go file.

The application logic works as follows. It connects to RDS instance and creates the database and the tables. Then insert the string (Hello from Graylog). If on next runs this is already in the database it query the string and display it as web page.
The application function main depends on environment variables for RDS endpoint, username and password to access the database. There is one function checkDB which bootstraps the DB server with creation of database and tables but without the data. The data was inserted in main function only one time and then it is queried by GET request. All of this logic is implemented in the go main() function.


## Considerations testing and approach taken


- Step 1 - First approach taking all in as simple as possible manner. Starting with one instance creating VPC adding application which does not connect tor RDS and see how it works.
- Step 2 - create vpc.tf, provider.tf (in aws-ec2-terraform), key.tf, securitygroup.tf,vars.tf,versions.tf. Building the application itself as simple as possible only to display the string. Building the minimal terrafrom files needed to deploy only one single EC2 instance.
The state file was local for now it has been stored in s3 but in a later step. The VPC was created with 3 public and 3 private subnets in eu-central-1 zone and respective internet gateway for the public subnets. Later for the private subnets it needs nat gateway. 

The application initial version was build to serve as simple web server which displays simple message string. The string (Hello Graylog) is not stored in the DB right now. Golang has ben chosen for building the application. Golang is not so hard to learn , very good for small but not only (k8s is golang) applications and tooling. One benefit is that it is compiled language with all the dependencies build in the executable file and in that case thinking for that application without using heavy frameworks like Django etc, it works well. Also dependencies for further connection to RDS could be backed in one single binary for easy deployment after. 
It has been chosen Gitlab because to be able to have build pipeline, which will produce the artifact (my binary file). 

- Step 3 - adding more complexity 
  a. build the application with Gitlab
  b. deploy test single instance with terraform with public IP and verify the application is working. (manually copy the application)
  c. thinking how the application could be copied automatically after finalizing the project.
  d. deploy the application to ASG in the public subnet for easy access during testing only. Verify all is working
  e. Repeat the same steps but changing application to talk to RDS. 
  f. investigating user_data possibility in single instance and Autoscaling group as well.

Creating of the terraform files like autoscaling.tf adding two instances adding nat.tf file for nat gateway for the private subnets (to be able instances to communicate with internet) and also deploying scenario with one jump server in the public subnet respective security groups and two instances in the private subnet.
user_data is the main way of deploying the application to the ASG in this design. This leads to the creation of "prereq" folder which can be run only to pre-provision dependencies. This creates one s3 bucket with two folders inside. The one called "terraform" is for  terraform state files which will be stored there and the other is to store the application binary and other dependencies like systemd servie file. 
Important step here is to create IAM policy which will allow the EC2 instances to freely communicate with the S3 buckets where our application binary will be stored. EC2 ideally in ASG will have user data script,  which will take care for deployment of the application copying files from s3 bucket to the local "/data" folder and run the application automatically.

- Step 4 - adding the complexity of external RDS service and the more security groups.
Deploying the application and integrate with RDS. Changing the application to be able to talk to the RDS instance and get the data. 
On the next step the rds.tf was created with modified userdata template in autoscaling.tf, passing information for the RDS instance from variables and also changing the instances to have EBS volume of 10 GB where the actual application will live with its environment variables.
The application itself accepts 3 environment variables RDS host:port, username, password. 

- Step 5 and 6
Next on almost the final step where after testing then adding additional files for autoscalingpolicy.tf for hypothetical CPU usage beond 70% to autoscale from 2 to 4 instances in all the three AZ eu-central-1a/1b/1c. Last step was to make application loadbalancer ALB in the public subnets and Target Group which automatically add the instances from the autoscaling group. The security groups also were adapted.


##  Detailed explaination of the terraform files for the whole solution

- Enter to the folder "aws-ec2-terraform". Inside there are all of the terraform files needed to deploy the application with the autoscaling group, RDS, Loadbalancesr, policies etc.
- vpc.tf - have the code for creating the vpc myVPC. resource aws_vpc creates the VPC , resource aws_subnet creates the subnets. From line 1 to line 12 is the VPC creation. From line 14 to line 45 public subnets creation (3 in 3 different AZ). From line 49 to line 80 it creates three private subnets again in different AZ. Line 82 to 90 is for resource "aws_internet_gateway" which is the internet Gateway needed for the public subnet to communicate to internet. From line 91 to line 103 - the routing table (resource "aws_route_table" ) which associates with the internet gateway - gateway_id = aws_internet_gateway.myVPC-gw.id. Then from line 104 to line 118 this is the association between a public routing table and public subnets. Also 0.0.0.0/0 is the route added with in igw (internet gateway) meaning use it as default gateway. 
- version.tf - this files sets verion terraform version >= 0.12.
- vars.tf - variables. AWS region "eu-central-1" is used ion this deployment. "mykey" is the private key for all of the EC2 instances. We need to generate this and I will show later. AMIS variable is important to use amazon AMI (Amazon Linux 2 AMI). This one in particular is used because all the tools needed like awscli are already installed there. Those AMI images are subject to change so please check before deploying in the particular region and adapt the image. You can do it by simply going to the console in EC2 and open Launch new instance wizard.
In this way you can see the AMI id and then change it in the map variable "AMIS". Then it will be picked later automatically if you change the region as well as variable "AWS_REGION".
variable "INSTANCE_DEVICE_NAME" - this is the additional EBS volume where the application will be copied for the particular instance.
variable "RDS_PASSWORD" - this is the password that will be asked for the RDS instance
variable "MARIADB_VERSION"  - version of the mariadb, 
variable "s3_bucket" - As a NOTE this should match to the same value from the "prereq" step where we create the bucket!!!
- securitygroup.tf - This is the important step where the security groups has been created. 

| Security Group  | inbound | outbound |
| -------------   | --------| ---------|
| asg-securitygroup | 22 from any, 8080 from alb-securitygroup | any |
| alb-securitygroup | 80 from any  | any |
| allow-ssh  | 22 from any | any |
| allow-mariadb  | 3306 from asg-securitygroup and allow-ssh | any |

The RDS and ASG instances are deployed in the Private subnets, but one jumpserver ahs been deployed for troubleshooting purposes only, which we can keep shut down if we don't need it.

asg-securitygroup - assigned to all of the insrtances (22 from any is not a security issue becuase they are in the private subnet)
alb-securitygroup assigned to the ALB
allow-ssh assigned to the jumphost (this could be security issue can be changed to myIP only from source or better keep shut down and only used if needed)
allow-mariadb - to allow port 3306 mysqld - in the private subnets

- rds.tf - This is the RDS (mariadb) service. Line 1 to 17 is the actual resource for subnet group and parameters group with default parameters except specified. The RDS is deployed in subnet_ids  = [aws_subnet.myVPC-private-1a.id, aws_subnet.myVPC-private-1b.id] in fact two private subnets from myVPC. Can be deployed in three in case of eu-central-1.
From line 18 "aws_db_instance" "mariadb" - this is the actual DB instance in RDS. 
The password is delivered from the varible mentioned. Also to improve the resiliency the "multi_az" can be set to "true". Also in other situations "instance_class" can be changed to bigger instance but for this particular case t2.micro is enough.
- provider.tf - this is the way to store the terraform.tfstate file in the same S3 bucket that has been created. It is stored in folder terraform. Change the region if you do the deployment in another than eu-central-1.

- nat.tf - This is the terraform file that is used for creation of NAT gateway for the private subnets. If the nat gateway is not created then the private subnets and the instances in the ASG will not be able to communicate to the internet. Line 1 to line 12 is the elastic IP and the nat gateway resource creation. From line 12 routing tables for the private subnets and also association between a private routing table and the nat gateway and also association with private subnets. (Basically 0.0.0.0/0 goes to nat gateway ans also three of the private subnets associates with this new routing table the same way as the public subnets assotiate with separate routing table and internet gateway.) - line 24 to 31.
Below is the exact block showing the association with the nat gateway.
```
route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gw.id
  }
``` 
- key.tf - There are two variables PATH_TO_PRIVATE_KEY and PATH_TO_PUBLIC_KEY. Here "aws_key_pair" "mykeypair"  is the public key used and injected in the EC2 instances for ssh access. Needs to be generated with ssh-keygen and will be explained in the deployments steps below.

- instance.tf - This contains creation only of one instance for jumpserver. The resource "aws_instance" "jumphost"  is doing that and is creating EC2 instance in the public subnet. This is an instance which can be used only for troubleshooting purposes further. It uses the same VPC and keypair.

- autoscaling.tf - This is the actual autoscaling group of instances which will be created to host our application. From line 1 to line 43 (resource "aws_launch_configuration") - this is the launch configuration. Same keypair is used and asg-security group also is used for the instances to be assigned to. Earlier in prereq section the IAM role was created "s3-mybucket-role" with a specific policy "s3-mybucket-role-policy" which allows access to the repo folder in the S3 bucket created in same prerequisite section. Here the mapping was done so the instances will be able to access the bucket and that folder. With  "ebs_block_device" 10 GB type gp2 hdd was added to the instances of the ASG.
user_data and is the key for that deployment. From line 15 to line 39.

a. Get the device created and get it with the variable from the vars file like "/dev/xvdh"
```
DEVICE=\${var.INSTANCE_DEVICE_NAME}
  FS_TYPE=$(file -s $DEVICE | awk '{print $2}')
  MOUNT_POINT=/data

  # If no FS, then this output contains "data"
  if [ "$FS_TYPE" = "data" ]
  then
    echo "Creating file system on $DEVICE"
    mkfs.xfs $DEVICE
  fi

```
b. mount it and add it persistently to the /etc/fstab. So mount the disk persistently to /data
```
mkdir $MOUNT_POINT
mount $DEVICE $MOUNT_POINT
echo "\${var.INSTANCE_DEVICE_NAME} /data xfs defaults 0 0" >> /etc/fstab

```
c. Copy the application myapp and the systemd unit files, chage to executable, application myapp is copied to /data.

```
aws s3 cp s3://${var.s3_bucket}/repo/myapp /data
chmod 755 /data/myapp
aws s3 cp s3://${var.s3_bucket}/repo/myapp.service /etc/systemd/system
```

d. Create the .env file with username password and RDS endpoint and then enable and start systemd unit called myapp.service.

```
echo -e "USERNAME=root\nPASSWORD=${var.RDS_PASSWORD}\nENDPOINT=${aws_db_instance.mariadb.endpoint}" >> /data/.envs
systemctl enable myapp.service 
systemctl start myapp.service 

```
What is happening the password has been taken from the variable on terraform apply stage and you have to put the value for the password for the RDS instance.
Then with ${aws_db_instance.mariadb.endpoint} the endpoint for the application (the FQDN for mariadb) + the port in form mariadbfqdn:port are passed to the /data/.envs.

From line 45 the resource "aws_autoscaling_group" is the actual ASG creation from the launch configuration.

There is one important code block at the end which basically tells to do not create the ASG and the instances before RDS and nat gateway are ready. Remember RDS and all of the instances except jumphost are created in the private subnets.


```
depends_on = [
    aws_nat_gateway.nat-gw,
    aws_db_instance.mariadb,
  ]

```
The ASG creates two instances initially with possibility to scale to 4 to have some HA and resiliency.

```
min_size                  = 2
max_size                  = 4
```

- autoscalingpolicy.tf - This creates autoscaling policy. In our previous configuration two instances in the two AZ are always online to have some resiliency and HA, but in case CPU increases beond 70 % new instances are automatically created and included in the ALB TG1(application load balaner target groups).
The policy creates aws CloudWatch alarm aws_cloudwatch_metric_alarm for CPU utilization > 70% for 120 sec (line 1 to 29.). There is also Scale down policy created when CPU goes below 8% for 120 sec. from line 30 to 56. CloudWatch alarm is triggered and metrics are used from there.

- alb.tf - this is the final tf file responsible for creation of ALB (application load balancer and the target group). From line 1 to line 17 and it is created cross all of the public subnets. With the statement load_balancer_type = "application" the loadblancer turns to type application ALB.
It listens to port 80 from Internet.
```
resource "aws_lb_listener" "alb-listener" {
  load_balancer_arn = aws_lb.my-alb.id
   port              = "80"
  protocol          = "HTTP"
```

From line 30 to line to line 35 the target group has been created for port 8080 because our application works on that port as also shown in the diagram. The resource "aws_autoscaling_attachment" from line 36 connects automatically the ASG with the target group so we don't need to add the backend servers manually and they are attached automatically.
The backend checks are used by default.




## Deployment steps

## Step 1 : - Create the deployment user and install git and awscli.

Deploy the terraform files in "prereq" folder. So please clone the project and go to the folder "prereq". Before anything else the terraform user is needed. In AWS in IAM go to users and create new user called for example terraform. Then add "AdministratorAccess" policy in permissions attach existing policies. ![Iam Policy1](images/IamTerraform1.JPG)  
![Iam Policy2](images/IamTerraform2.JPG). Other possibility instead attaching policy directly is to create a group and then add the user member of that group and then attach the same policy to the group.
![Iam Policy3](images/IamTerraform3.JPG)
![Iam Policy4](images/IamTerraform4.JPG)
awscli is also need to be installed. Below is the link for how to install awscli on windows, Linux or MAC. The user for terraform needs to have only programmatic access and nothing else then you can add the AWS KEY and Secret in the config file (do not share or commit the credentials). 
```
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html
```

For to install git please see instructions here https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

```
mkdir projectdeploy
git clone https://gitlab.com/dkrasimirov/niceproject.git
```

   Inside the main repositiry there is main.go application. If you open it you will see how it functions.
From line 12 to line 28 func checkDb works as a bootstrap function if the RDS(Mysql) database and tables does not exists. If they exists this function does not do anything. It is just for initializing the application on the first run. From line 31 on this is the main fuction where it accepts environment variables for user , password and RDS endpoint and then if the database and the tables related are there it just query and deliver the string. There is an if statement which runs only once the insert of the string and then en every run this goes to else statement where it just queries the table and pass the string to the web server to display it.


## Step 2 - Deploy the prerequisite S3 bucket

- Then go to the prereq folder inside the niceproject folder. s3.tf creates s3 bucket with the name of the variable s3_bucket in vars.tf. 
- NOTE: if you change this variable value because s3 bucket names are globally unique then change the same variable in "aws-ec2-terraform/vars.tf, this is very important. Because otherwise the actual user_data will not be able to copy the files. Lines 1 to 8 creates bucket line 11 to 25 are related to removing public access from the bucket. Last two resources aws_s3_bucket_object is to create sub folders inside S3, called "terraform" for storing tfstate files and "repo" for storing the application and systemd service file.
- vars.tf - contains the region environment variable ad the s3 bucket name.
- provider.tf - aws terraform provider and the var.AWS_REGION (eu-central-1) in this case
- iam.tf containes important policy for EC2 instances to be able to access the S3 bucket. The IAM role is called s3-mybucket-role.
```
###inside "prereq" folder 
terraform init      //- to initilize
terraform validate   //- to validate for syntax errors
terraform plan      //- to see what is going to change
terraform apply
```
- terraform init
```
Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v3.70.0...
- Installed hashicorp/aws v3.70.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

- terraform validate
```
Success! The configuration is valid.
```
- terraform plan
```
      + block_public_acls       = true
      + block_public_policy     = true
      + bucket                  = (known after apply)
      + id                      = (known after apply)
      + ignore_public_acls      = false
      + restrict_public_buckets = true
    }

Plan: 7 to add, 0 to change, 0 to destroy.
```
- terraform apply
```
Plan: 7 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

```
```
aws_s3_bucket_object.repo: Creating...
aws_iam_instance_profile.s3-mybucket-role-instanceprofile: Creation complete after 2s [id=s3-mybucket-role]
aws_s3_bucket_object.repo: Creation complete after 1s [id=repo/]
aws_s3_bucket_object.tf: Creation complete after 1s [id=terraform/]
aws_s3_bucket_public_access_block.baccess: Creation complete after 1s [id=dansbucketrepo80]

Apply complete! Resources: 7 added, 0 changed, 0 destroyed.

```


Verify in AWS console in S3 there is a bucket and the two mentioned subfolders. This has been shown on the following screeshot 
![S3 Bucket](images/s3_prereq-bucket.jpg).

- Wait a bit if the folders are not immediately there.

- After the bucket creation please do the following.
  1. Download on your desktop the latest build of the application from the following link. https://gitlab.com/dkrasimirov/niceproject/-/pipelines.
  In this case pick latest sucessfull pipeline and download the artifact zip file which contains the application called myapp.
  2. Copy from the clone of the mainb repository a file called myapp.service to the same place you have dounload the artifact zip for myapp. You can also download it from the main repository.
  3. Unzip the artifacts.zip file in the current directory.
  4. Upload "myapp" and "myapp.service" to the repo folder in the s3 bucket created previously with terraform.
![myappdown](images/myappDown.JPG)
![servicedown](images/myappservice1.JPG)
![servicedown1](images/muappservice.JPG)
![myapp1](images/myapps3-1.JPG)
![myapp2](images/myapps3-2.JPG)
![myapp3](images/myapps3-3.JPG)
![myapp4](images/myapps3-4.JPG)
![myapp5](images/myapps3-5.JPG)



# Step 3 application Deployment

- If you haven't cloned the repository yet, please check step 1 and 2 and navigate to folder "aws-ec2-terraform".
- Create the ssh key called mykey ( this can be cahnged in vars.tf). 
```
cd aws-ec2-terraform
ssh-keygen -f mykey -b 4096

```
- Issue the commands again for terraform init validate and plan inside folder "aws-ec2-terraform". The versions in your case could be slightly different.
```
terraform init
terraform validate
terraform plan
terraform apply
```

- terrafrom init
```
Initializing the backend...

Successfully configured the backend "s3"! Terraform will automatically
use this backend unless the backend configuration changes.

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v3.70.0...
- Installed hashicorp/aws v3.70.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```
- terraform plan

```
terraform plan
var.RDS_PASSWORD
  Enter a value: 

      }
      + tags_all                       = {
          + "Name" = "myVPC"
        }
    }

Plan: 37 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + ELB      = (known after apply)
  + RDS      = (known after apply)
  + jumphost = (known after apply)

```

- terraform apply
```
terraform apply
var.RDS_PASSWORD
  Enter a value: your RDS password

---
Plan: 37 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + ELB      = (known after apply)
  + RDS      = (known after apply)
  + jumphost = (known after apply)

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes
```


If the remote S3 bucket has not been already created you will receive and error because the tf state file could not be deployed in the folder "terraform"

The terraform plan and terraform apply will ask for the root password for the RDS instance. Please put complex password and store it in some keyvault or password manager solution.
The terraform plan and apply will show that approx 37 new resources will be created.
Then wait all of the resources to be created it will take some time.

Normally the terraform apply will have output for ALB DNS name, RDS endpoint , jumphost. Please watch the output when the terraform apply finishes for example.
(the output in your case will be slightly different)
```
Apply complete! Resources: 37 added, 0 changed, 0 destroyed.

Outputs:

ELB = "my-alb-xxxxxxxx.eu-central-1.elb.amazonaws.com"
RDS = "mariadb.xxxxxxxxxx.eu-central-1.rds.amazonaws.com:3306"
jumphost = "some ip address"
```
Copy the above in text editor you might need it after.


Please navigate either through the web portal or check the output from terraform but you can find in EC2 -> LoadBalancers -> my-alb -> DNS name

## Verify the application
Copy the DNS name (From the output ELB = ) and paste it in a new browser window to verify that the application is working. Please wait a bit before and check that the targets in the target group TG1 are healthy. The web page should return "Hello from Graylog"

The screenshot below shows how to copy the url of the application from AWS management console. (in the EC2 -> Load balancers section)

![verifyDNS](images/LBDNSName.JPG)


Do not forget to stop the jumphost as it is not needed.

# Step 4 troubleshooting

If the instances are not heathy on the target group, it means that something happen and the application did not start.
Possible problems could be the creation of the data mount point , aws s3 copy did not suceed or missing aws cli tools. Other posibility is that systemd service does not grab .envs or .envs is empty.

- so first is to use ssh agent. From linux workstation or WSL on windows for example use ssh and ssh-agent. The way to connect to the one of the node of the ASG is:

```
eval `ssh-agent -s`
ssh-add mykey
ssh -A ec2-user@IP_address_Of_The_jumphost // you can go to EC2 instances and get the public IP address of the jumphost,or from terraform output after terraform apply.
```
- Verify the status of the TG1 (the ALB target Group)
- from the same EC2 console in AWS web interface please note the ip address of one of the instances or the instance which is not healthy in the target group.

then ssh to the private ip address

![Verify1](images/verify1.JPG)


![Verify3](images/verify3.JPG)

```
ssh ec2-user@asg_instance_private_ip

```
- to make some checks if the /data has been mounted

```
df -h
Filesystem      Size  Used Avail Use% Mounted on
devtmpfs        474M     0  474M   0% /dev
tmpfs           483M     0  483M   0% /dev/shm
tmpfs           483M  468K  483M   1% /run
tmpfs           483M     0  483M   0% /sys/fs/cgroup
/dev/xvda1      8.0G  1.5G  6.5G  19% /
/dev/xvdh        10G   51M   10G   1% /data
tmpfs            97M     0   97M   0% /run/user/0
tmpfs            97M     0   97M   0% /run/user/1000

```
- to check the application

```
cd /data
ls -la
drwxr-xr-x  2 root root      32 Dec 18 16:10 .
dr-xr-xr-x 19 root root     269 Dec 18 16:10 ..
-rw-r--r--  1 root root     101 Dec 18 16:10 .envs
-rwxr-xr-x  1 root root 8403459 Dec 18 10:30 myapp

```
- If myapp is not there the .env file is empty or the /data is not mounted then clearly either the original tf files were changed or the user_data has been changed.
if you have changed the name of the s3 bucket in the "prereq" folder you have to put the same string value in the variable "s3_bucket" in "aws-ec2-terraform" vars.tf.

you can also install mysql client yum -y install mysql and also telnet the RDS endpoint on port 3306, eventually to check if there is connectivity to 3306 to the RDS instance.

- one last step to troubleshoot is to do sudo systemctl stop myapp and then cd in /data and run myapp manually. 
```
./myapp
```
this will throw an error if it cannot connect to the RDS instance.

One great feature of ASG is that if one of the nodes start to mailfuction it can be terminated and ASG will create new replacement instance.

# Destroy 

- destry the solution run below command from inside first "aws-ec2-terraform" and then to destroy the s3 after that you can go inside the "prereq" folder

```
terraform destroy
```






